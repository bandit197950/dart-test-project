import 'dart:convert';
import 'dart:io' show HttpException;

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_router/angular_router.dart';
import 'package:chat_models/chat_models.dart';
import 'package:chat_web/routes.dart';
import 'package:chat_web/services.dart';
import 'package:web_socket_channel/html.dart';

@Component(selector: 'chat', templateUrl: 'chat_component.html', styleUrls: [
  'chat_component.css'
], directives: [
  coreDirectives,
  materialInputDirectives,
  MaterialButtonComponent,
  MaterialCheckboxComponent,
  MaterialIconComponent
], pipes: [
  DatePipe
], providers: [
  materialProviders,
  ClassProvider(WebMessagesClient),
  ClassProvider(WebChatsClient)
])
class ChatComponent implements OnActivate, OnDeactivate {
  final maxMessages = 15;
  WebMessagesClient messagesClient;
  WebChatsClient chatsClient;
  Router router;
  Session session;
  ChatId chatId;
  Chat chat;
  HtmlWebSocketChannel webSocketChannel;
  List<Message> messages;
  List<Message> shrinkedMessages;
  String newMessageText = '';
  bool showAllMessages = false;

  ChatComponent(this.messagesClient, this.chatsClient, this.router, this.session);

  send() async {
    final newMessage = Message(
        chat: chatId,
        author: session.currentUser,
        text: newMessageText,
        createdAt: DateTime.now());
    try {
      final createdMessage = await messagesClient.create(newMessage);
      messages.add(createdMessage);
      _shrinkMessagesList();
    } on HttpException catch (e) {
      print('Sending message failed');
      print(e);
    }

    newMessageText = '';
  }

  toChatList() {
    router.navigate(RoutePaths.chats.toUrl());
  }

  String chatMembers() => chat
      ?.members
      ?.map((user) => user.name)
      ?.join(', ');

  @override
  onActivate(_, RouterState current) async {
    chatId = ChatId(current.parameters['chatId']);
    webSocketChannel = HtmlWebSocketChannel.connect('ws://localhost:3333/ws');
    webSocketChannel.sink.add(json.encode(chatId.json));
    _receiveMessages(chatId);
    _receiveChat(chatId);
  }

  @override
  onDeactivate(RouterState current, _) {
    webSocketChannel.sink.close();
  }

  _receiveMessages(ChatId chatId) async {
    messages = await messagesClient.read(chatId);
    webSocketChannel.stream.listen((data) {
      final recievedMessage = Message.fromJson(json.decode(data));
      if (recievedMessage.author.id != session.currentUser.id) {
        messages.add(recievedMessage);
        _shrinkMessagesList();
      }
    });
    _shrinkMessagesList();
  }

  _receiveChat(ChatId chatId) async {
    try {
      chat = await chatsClient.read(chatId);
    } on HttpException catch(e) {
      print('Getting chat failed');
      print(e);
    }
  }

  _shrinkMessagesList() {
    shrinkedMessages = messages;
    if(showAllMessages == false && messages.length > maxMessages)
      shrinkedMessages = shrinkedMessages.sublist(messages.length - maxMessages, messages.length);
    print('shrinked');
  }

  showAllMessagesChanged(event) {
    showAllMessages = !showAllMessages;
    _shrinkMessagesList();
  }
}
